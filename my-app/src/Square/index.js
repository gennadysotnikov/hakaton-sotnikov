import React from "react";
import "./styles.css";

const Square = ({value}) => {
  return <div className="square_cont">{value > 0 ? value : ""}</div>
};

export default Square;
