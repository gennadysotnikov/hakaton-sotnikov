import React, { useState } from "react";
import Square from "../Square";
import "./styles.css";

const initCol = Math.round(0 - 0.5 + Math.random() * (3 - 0 + 1));
const initRow = Math.round(0 - 0.5 + Math.random() * (3 - 0 + 1));

const initialField = [

];

const GameField = () => {
  let initialField = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
  initialField[initRow][initCol] = Math.round(Math.random()) === 1 ? 4 : 2;
  const [field, setField] = useState(initialField);
  return (
    <div className="container">
      {field.map((row, rInd) => {
        return row.map((el,cInd) => (
          <>
            <Square key={`${rInd}${cInd}`} value={field[rInd][cInd]} />
            {(cInd === 3) && <br/>}
          </>
        ))
      })}
    </div>
  );
};

export default GameField;
