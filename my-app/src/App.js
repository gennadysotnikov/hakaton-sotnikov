import React, { useReducer } from 'react';
import GameField from "./GameField";
import { reducer, initialState, initialize } from "./reducer";
import './App.css';

function App() {
  const [state, dispatch] = useReducer(reducer, initialState, initialize);
  window.addEventListener("keydown", evt => {
    console.log(evt)
    dispatch({type: evt.key})
  });
  return (
    <div className="App">
      <GameField />
    </div>
  );
}

export default App;
