export const initialState = {
  field: [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
};

export const initialize = () => {
  return { ...initialState }
};

export const reducer = (state = initialState, action) => {
  console.log("action.type", action.type)
  switch (action.type) {
    case "ArrowRight": {
      console.log("r")
        break;
    }
    case "ArrowLeft": {
      console.log("l")
      break;
    }
    case "ArrowUp": {
      console.log("u")
      break;
    }
    case "ArrowDown": {
      console.log("d")
      break;
    }
    default: return state;
  }
};
